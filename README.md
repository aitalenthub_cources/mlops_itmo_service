# Задания по курсу Технологии и практики MLOps
Данный репозиторий предназначен для сервинга моделей

## Команда
Фёдорова Инесса

## Модели подгружаются из registry MLFlow

![image](imgs/mlflow.png)

## Настроен Мониторинг

![image](imgs/monitoring.png)


## GitLab Registry

Собранные образы хранятся в gitlab registry, сценарий ci/cd можно увидеть в файле `.gitlab-ci.yaml`


## Сервис

Сервис, предназначенный для матчинга резюме и вакансий, обернут в FastAPI и лежит по адресу http://92.246.76.194:8010/docs


```bash
docker-compose -f docker-compose.yaml up -d
```

![image](imgs/service1.png)

![image](imgs/service2.png)

