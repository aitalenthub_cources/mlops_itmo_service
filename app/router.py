import os

import numpy as np
from fastapi import APIRouter
from fastapi.responses import JSONResponse
from fastapi import status

from embedder import Embedder


router = APIRouter(tags=["embedder"])
embedder = Embedder(str(os.getenv("MODEL_NAME")))


@router.get("/")
async def read_root():
    """Displays greetings"""
    return {"Greetings": "Welcome to our service"}


@router.post("/embed_text")
async def embed_text(user_text: str):
    """
    Process the user's text and
    return a JSON response.

    Args:
        user_text (str): The text provided by
        the user that needs to be processed.

    Returns:
        JSON response: A JSON response object with a status
        code of 200 and the content is the result of calling
        the `answer` method of the `Embedder` instance.
    """
    embed = embedder.answer(user_text)

    return JSONResponse(
        status_code=status.HTTP_200_OK, content=embed
    )


@router.post("/compute_similarity")
async def compute_similarity(job_text: str, cv_text: str):
    """
    Process the user's text and
    return a JSON response.

    Args:
        user_text (str): The text provided by
        the user that needs to be processed.

    Returns:
        JSON response: A JSON response object with a status
        code of 200 and the content is the result of calling
        the `answer` method of the `Embedder` instance.
    """
    embed_job = np.array(embedder.answer(job_text)['query_embedding'])
    embed_cv = np.array(embedder.answer(cv_text)['query_embedding'])
    similarity = embed_job @ embed_cv.T

    return JSONResponse(
        status_code=status.HTTP_200_OK, content=similarity
    )
